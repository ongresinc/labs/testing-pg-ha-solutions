
## Run PoC environment

### Repmgr 3.3 PoC

```
mvn package -P poc-repmgr-3.3
```

### Repmgr 4.1.0 PoC

```
mvn package -P poc-repmgr-4.1.0
```

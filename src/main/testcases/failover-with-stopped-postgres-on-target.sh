#!/bin/bash

. "$(dirname "$0")/common.sh"

configure-by-repmgr-version
wait-repmgr-active pg-1 4
monitor-and-check-failover-from pg-1 &
monitor-pgbouncer &
wait-pgbouncer-monitor-running
stop-postgres pg-2
stop-postgres pg-1
wait %1
sleep 5
kill %2
#!/bin/bash

. "$(dirname "$0")/common.sh"

check-root

configure-by-repmgr-version
wait-repmgr-active pg-1 4
(monitor-and-check-failover-from pg-1 && monitor-and-check-master pg-3) &
monitor-pgbouncer &
wait-pgbouncer-monitor-running
network-failure --failure-type nodes --failure-target pg-1,pg-2 &
wait %1 %3
sleep 60
kill %2
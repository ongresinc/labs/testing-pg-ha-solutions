#!/bin/bash

. "$(dirname "$0")/common.sh"

configure-by-repmgr-version
wait-repmgr-active pg-1 4
monitor-pgbouncer &
wait-pgbouncer-monitor-running
stop-postgres pg-1
wait-repmgr-active pg-2 3
repmgr-standby-clone-and-follow pg-1 pg-2
monitor-and-check-failover-from pg-2 &
stop-postgres pg-2
wait %2
sleep 5
kill %1
#!/bin/bash

set -e

PGPORT="$1"
shift

date
echo "Checking if $(hostname) is master for repmgr:"

if [ "$REPMGR_VERSION" == "3.3" ]
then
  masters=($(/usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d repmgr -A -t -c "SELECT name FROM repmgr_main.repl_nodes WHERE type='master' AND active != 'f'"))
else
  masters=($(/usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d repmgr -A -t -c "SELECT node_name FROM repmgr.nodes WHERE type='primary' AND active != 'f'"))
fi

if [ "${#masters[@]}" -eq 1 ] && [ "${masters[0]}" == "$(hostname)" ]
then
  echo "$(hostname) is master"
  exit 0
fi

>&2 echo "$(hostname) is not master"
exit 1
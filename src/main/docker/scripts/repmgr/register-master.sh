#!/bin/bash

set -e

PGPORT="$1"
PGDATA="/var/lib/postgresql/data-$PGPORT"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift

/usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d template1 -c "DROP DATABASE IF EXISTS repmgr"
/usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d template1 -c "CREATE DATABASE repmgr"
export ID=$(hostname|sed 's/pg-/1/')
bash /scripts/repmgr/repmgr-config.sh "$PGPORT" > "$REPMGRCONF"
chown postgres:postgres "$REPMGRCONF"
su postgres -c "/usr/local/pgsql/bin/repmgr -f '$REPMGRCONF' master register -F"

#!/bin/bash

set -e

PGPORT="$1"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift

if [ ! -f "$REPMGRCONF" ]
then
  >&2 echo "$REPMGRCONF not found!"
  exit 1
fi

if [ ! -f /var/run/repmgrd-$PGPORT.pid ] || ! kill -0 "$(cat /var/run/repmgrd-$PGPORT.pid)" \
  || [ "$(ps --pid "$(cat /var/run/repmgrd-$PGPORT.pid)" -o stat|tail -n +2)" == "Z" ]
then
  > /var/log/repmgr-$PGPORT.log
  > /var/run/repmgrd-$PGPORT.pid
  chown postgres:postgres /var/run/repmgrd-$PGPORT.pid /var/log/repmgr-$PGPORT.log
  su postgres -c "/usr/local/pgsql/bin/repmgrd -d -f '$REPMGRCONF' -p /var/run/repmgrd-$PGPORT.pid"
fi

#!/bin/bash

set -e

PGPORT="$1"
PGDATA="/var/lib/postgresql/data-$PGPORT"
REPMGRCONF="/var/lib/postgresql/repmgr-$PGPORT.conf"
shift

if [ ! -f "$REPMGRCONF" ]
then
  >&2 echo "$REPMGRCONF not found!"
  exit 1
fi

su postgres -c "/usr/local/pgsql/bin/repmgr -f '$REPMGRCONF' -F standby follow"

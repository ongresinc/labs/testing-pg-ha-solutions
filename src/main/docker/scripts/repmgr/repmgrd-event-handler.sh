#!/bin/bash

set -e

PGPORT="$1"
shift
NODE_ID="$1"
shift
EVENT_TYPE="$1"
shift
SUCCESS="$1"
shift
TIMESTAMP="$1"
shift
DETAILS="$1"
shift

date
echo "Event node:$NODE_ID type:$EVENT_TYPE success:$SUCCESS timestamp:$TIMESTAMP details:$DETAILS"
echo

if [ "$EVENT_TYPE" == "repmgrd_failover_promote" ]
then
  OLD_MASTER="$(echo "$DETAILS"|sed 's/.*old \(master\|primary\) \([^ ]\+\) marked as failed.*/\2/')"
  curl -s "http://localhost:8500/v1/kv/repmgrpoc/$PGPORT/failed_masters/$OLD_MASTER" -X PUT
fi
#!/bin/bash

PGPORT="$1"
shift

> /var/log/postgresql-service-check.log

script="/tmp/repmgr-check-master-$PGPORT.sh"
echo "bash /scripts/repmgr/repmgr-check-master.sh $PGPORT" > "$script"
chmod a+x "$script"
script2="/tmp/handle-failed-master-$PGPORT.sh"
echo "bash /scripts/consul/handle-failed-master.sh $PGPORT" > "$script2"
chmod a+x "$script2"

echo '{
  "service": {
    "name":"postgresql",
    "address":"'"$(hostname)"'",
    "port":'"$PGPORT"',
    "checks": [ {
      "script":"'"$script"'",
      "interval":"10s"
    } ]
  },
  "watches": [ {
    "type":"keyprefix",
    "prefix":"/repmgrpoc/'"$PGPORT"'/failed_masters/",
    "handler":"'"$script2"'"
  } ]
}' > /var/lib/consul/config/postgresql.json

[ ! -f /var/run/consul.pid ] || kill -1 $(cat /var/run/consul.pid) 2>/dev/null
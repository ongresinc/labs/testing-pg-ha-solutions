#!/bin/bash

set -e

PGPORT=$1
shift

PGDATA=/var/lib/postgresql/data-$PGPORT
echo "Stopping postgres instance on port $PGPORT"
  
nohup sh > /dev/null 2>&1 << EOF &
  exec su postgres -c "/usr/local/pgsql/bin/pg_ctl start -D $PGDATA"
EOF

#!/bin/bash

set -e
export port="$1"
export PGDATA=/var/lib/postgresql/data-$port

echo "Break postgres start"
mv $PGDATA $PGDATA.bak

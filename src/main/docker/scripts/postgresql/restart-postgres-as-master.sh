#!/bin/bash

set -e

PGPORT="$1"
PGDATA=/var/lib/postgresql/data-$PGPORT
shift
export master_host="$1"
shift

echo "Starting postgres master instance on port $PGPORT"

rm -Rf $PGDATA || true
echo "Waiting for backup of $PGPORT..."
mkdir -p $PGDATA
chown postgres:postgres $PGDATA
chmod 700 $PGDATA
while ! nc ${master_host} 15432 2>/dev/null | tar xC $PGDATA 2>/dev/null
do
  sleep 0.3
done

echo "Enabling recovery"
echo "
recovery_target_timeline='latest'
standby_mode = on
primary_conninfo = 'host=${master_host} port=$PGPORT'" > $PGDATA/recovery.conf
chown postgres:postgres $PGDATA/recovery.conf
[ ! -f "$PGDATA/recovery.conf" ] || mv "$PGDATA/recovery.conf" "$PGDATA/recovery.done"

chmod a+w /var/log

nohup sh > /dev/null 2>&1 << EOF &
  exec su postgres -c "/usr/local/pgsql/bin/pg_ctl start -D $PGDATA"
EOF

while [ ! -f /var/log/postgres-$PGPORT.log ]; do sleep 1; done
tail -f /var/log/postgres-$PGPORT.log &
echo $! > /var/run/tail-$PGPORT.pid

while ! /usr/local/pgsql/bin/psql -U postgres -p $PGPORT -d template1 -c "SELECT 1" 1>/dev/null 2>&1; do sleep 0.3; done;
echo "All postgres master are ready!";

kill $(cat /var/run/tail-$PGPORT.pid) 2>/dev/null || true

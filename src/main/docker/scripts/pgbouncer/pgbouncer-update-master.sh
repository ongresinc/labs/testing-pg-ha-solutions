#!/bin/bash

set -e

PGBPORT="$1"
shift
PGPORT="$1"
shift

data=$(cat)

date
echo "Watch output was:"
echo "$data"|jq '.[]|.Node.Node'
echo

healthy_agents="$(echo "$data"|jq '.[]|select(.Checks[]|(.CheckID=="serfHealth" and .Status=="passing"))'|jq -s .)"
echo "Healthy agents:"
echo "$healthy_agents"|jq '.[]|.Node.Node'
echo
masters="$(echo "$healthy_agents"|jq '.[]|select(.Checks[]|(.CheckID=="service:postgresql" and .Status=="passing"))'|jq -s .)"
echo "Masters:"
echo "$masters"|jq '.[]|.Node.Node'
echo

if [ ! -z "$masters" ] && [ "$(echo "$masters"|jq 'length')" -eq 1 ]
then
  node="$(echo "$masters"|jq -r '.[0].Node.Node')"
  echo "Pointing pgbouncer to master $node"
  bash /scripts/pgbouncer/update-pgbouncer.sh $PGBPORT "$node" $PGPORT
  exit 0
fi

if [ ! -z "$masters" ]
then
  if [ "$(echo "$masters"|jq 'length')" -gt 1 ]
  then
    >&2 echo "Not found just 1 master but $(echo "$masters"|jq 'length'): '$(echo "$masters"|jq 'join(.[].Node.Node)')'"
  else
    >&2 echo "Not found any master"
    exit 1
  fi
else
  >&2 echo "Not found any master"
  exit 1
fi

bash /scripts/pgbouncer/stop-pgbouncer.sh 6432
exit 1



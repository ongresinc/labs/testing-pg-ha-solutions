#!/bin/bash

set -e

PGBPORT="$1"
PGHOST="$2"
PGPORT="$3"

PGBDATA="/var/lib/pgbouncer/data-$PGBPORT"

useradd postgres||true

rm -Rf "$PGBDATA"
mkdir -p "$PGBDATA"

bash /scripts/pgbouncer/pgbouncer-config.sh "$PGBPORT" "$PGHOST" "$PGPORT" > "$PGBDATA/pgbouncer.ini"

> "/var/log/pgbouncer-$PGBPORT.log"

chown postgres:postgres /var/lib/pgbouncer/ "/var/log/pgbouncer-$PGBPORT.log" -R

if [ ! -f $PGBDATA/pgbouncer.pid ] || ! kill -0 "$(cat $PGBDATA/pgbouncer.pid)" \
  || [ "$(ps --pid "$(cat $PGBDATA/pgbouncer.pid)" -o stat|tail -n +2)" == "Z" ]
then
  su postgres -c "pgbouncer -d '$PGBDATA/pgbouncer.ini'"
fi

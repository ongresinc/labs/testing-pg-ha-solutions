/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

import com.ongres.gitlab.bot.Main.SubCommand;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.StringReader;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObject;

@Command(name = "testcase-restart-instance", 
    description = "Run a test case that stop a random instance and then restart it")
public class TestcaseRestartInstance extends SubCommand {
  
  @Option(names = {"--jdbc-url-template"},
      description = "The JDBC URL template.")
  protected String jdbcUrlTemplate = 
      "jdbc:postgresql://${address}:${port}/${database}?user=${user}&password=${password}";
  
  @Option(names = {"--jdbc-database"},
      description = "The JDBC database.")
  protected String jdbcDatabase = "postgres";
  
  @Option(names = {"--jdbc-user"},
      description = "The JDBC user.")
  protected String jdbcUser = "postgres";
  
  @Option(names = {"--jdbc-password"},
      description = "The JDBC password.")
  protected String jdbcPassword = "trustme";
  
  @Option(names = {"--container"},
      description = "The container name.")
  protected String container;
  
  @Option(names = {"--restart-as"},
      description = "The container name.")
  protected RestartAs restartAs;
  
  @Option(names = {"--follow-container"},
      description = "The container name to follow.")
  protected String followContainer;
  
  public enum RestartAs {
    master, standby, stopped;
  }
  
  @Override
  public void run() {
    Random random = new Random();
    
    String port = "5432";
    RestartAs restartAs = this.restartAs != null ? this.restartAs
        : RestartAs.values()[random.nextInt(RestartAs.values().length)];
    
    List<JsonObject> containers = Json.createReader(new StringReader(
        parent.waitProcessAndGet("docker inspect " 
            + parent.waitProcessAndGet("docker ps -q").replace("\n", " "))))
        .read()
        .asJsonArray()
        .stream()
        .map(entry -> entry.asJsonObject())
        .filter(entry -> entry
            .getJsonObject("NetworkSettings")
            .getJsonObject("Networks")
            .containsKey("repmgrpoc_network"))
        .collect(Collectors.toList());
    
    JsonObject container;
    if (this.container != null) {
      container = containers
          .stream()
          .filter(entry -> entry.getString("Name").equals("/repmgrpoc_" + this.container + "_1"))
          .findFirst()
          .orElseThrow(() -> new TestcasePreconditionsException(
              "Container " + this.container + " not found"));
    } else {
      container = containers.stream()
          .filter(entry -> entry.getString("Name").matches("/repmgrpoc_pg-[0-9]+_1"))
          .findFirst()
          .orElseThrow(() -> new TestcasePreconditionsException("No container found"));
    }
    
    System.out.println("Restarting instance as " + restartAs.name() 
        + " in container " + container.getJsonObject("NetworkSettings")
        .getJsonObject("Networks")
        .getJsonObject("repmgrpoc_network")
        .getString("IPAddress"));
    
    Optional<String> primaryAddress = followContainer == null 
        ? parent.getMasterFromRepMgrThroughConsul()
            : containers.stream()
            .filter(entry -> entry.getString("Name")
                .equals("/repmgrpoc_" + this.followContainer + "_1"))
            .map(entry -> entry.getJsonObject("NetworkSettings")
                .getJsonObject("Networks")
                .getJsonObject("repmgrpoc_network")
                .getString("IPAddress"))
            .findFirst();
    
    if (restartAs == RestartAs.standby 
        && !primaryAddress.isPresent()) {
      System.err.println("Container to follow not found, can not restart as standby");
      return;
    }
    
    parent.waitProcessAndGet("docker exec -t " + container.getString("Id") 
        + " bash /scripts/postgresql/stop-" + parent.abbreviatedType("postgresql") + ".sh"
        + " " + port);
    
    String backupSupplierAddress = containers.stream()
        .filter(entry -> entry.getString("Name").equals("/repmgrpoc_pg-1_1"))
        .findFirst()
        .orElseThrow(() -> 
          new TestcaseException("Backup supplier container repmgrpoc_pg-1_1 not found"))
        .getJsonObject("NetworkSettings")
        .getJsonObject("Networks")
        .getJsonObject("repmgrpoc_network")
        .getString("IPAddress");
    
    if (restartAs != RestartAs.stopped) {
      parent.waitProcessAndGet("docker exec -t " + container.getString("Id")
        + " bash /scripts/postgresql" 
        + "/restart-" + parent.abbreviatedType("postgresql") 
        + "-as-" + restartAs.name() + ".sh " + 5432 
        + (restartAs == RestartAs.master ? "" : " " + primaryAddress.get()) 
        + " " + backupSupplierAddress);
    }
  }
}

/*
 * TODO: generate License
 */
package com.ongres.gitlab.bot;

import com.ongres.gitlab.bot.Main.SubCommand;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObject;

@Command(name = "monitor-primary-instance", 
    description = "Monitor primary instance, trying to detect inconsistencies")
public class MonitorPrimaryInstance extends SubCommand {
  
  @Option(names = {"--jdbc-url-template"},
      description = "The JDBC URL template.")
  protected String jdbcUrlTemplate = 
      "jdbc:postgresql://${address}:${port}/${database}?user=${user}&password=${password}";
  
  @Option(names = {"--jdbc-port"},
      description = "The JDBC port.")
  protected Integer jdbcPort = 5432;
  
  @Option(names = {"--jdbc-database"},
      description = "The JDBC database.")
  protected String jdbcDatabase = "postgres";
  
  @Option(names = {"--jdbc-user"},
      description = "The JDBC user.")
  protected String jdbcUser = "postgres";
  
  @Option(names = {"--jdbc-password"},
      description = "The JDBC password.")
  protected String jdbcPassword = "trustme";
  
  @Option(names = {"--target"},
      description = "The target.")
  protected String target;
  
  @Option(names = {"--interval"},
      description = "The interval in milliseconds between checks.")
  protected int interval = 300;
  
  @Override
  public void run() {
    while (true) {
      try {
        if (this.target == null) {
          System.out.println("Update primary instance using RepMgr "
              + "each " + interval + " milliseconds");
        } else {
          System.out.println("Update instance " + this.target + " "
              + "each " + interval + " milliseconds");
        }
        
        List<JsonObject> containers = Json.createReader(new StringReader(
            parent.waitProcessAndGet("docker inspect " 
                + parent.waitProcessAndGet("docker ps -q").replace("\n", " "))))
            .read()
            .asJsonArray()
            .stream()
            .map(entry -> entry.asJsonObject())
            .filter(entry -> entry
                .getJsonObject("NetworkSettings")
                .getJsonObject("Networks")
                .containsKey("repmgrpoc_network"))
            .collect(Collectors.toList());
        
        Integer number = null;
        String available = null;
        
        while (true) {
          Optional<String> foundContainer = target != null 
              ? Optional.of("repmgrpoc_" + this.target + "_1") 
              : parent.getMasterFromRepMgrThroughConsul();
          
          if (!foundContainer.isPresent()) {
            if (available != null) {
              System.out.println("Primary unavailable");
              available = null;
            }
            
            continue;
          }
          
          String container = foundContainer.get();
          container = container.substring("repmgrpoc_".length(), container.length() - "_1".length());
          
          if (available == null || !available.equals(container)) {
            System.out.println("Primary available: " + container);
            available = container;
          }
          
          String address = containers.stream()
              .filter(entry ->  entry
                  .getString("Name").equals("/repmgrpoc_" + this.target + "_1"))
              .map(entry -> entry
                  .getJsonObject("NetworkSettings")
                  .getJsonObject("Networks")
                  .getJsonObject("repmgrpoc_network")
                  .getString("IPAddress"))
              .findFirst()
              .orElseThrow(() -> new IllegalStateException());
          String jdbcUrl = jdbcUrlTemplate
              .replace("${address}", address)
              .replace("${port}", String.valueOf(jdbcPort))
              .replace("${database}", jdbcDatabase)
              .replace("${user}", jdbcUser)
              .replace("${password}", jdbcPassword);
          Connection connection;
          try {
            Properties properties = new Properties();
            properties.setProperty("loggerLevel", "OFF");
            connection = DriverManager.getConnection(jdbcUrl, properties);
            
            try {
              try (Statement statement = connection.createStatement()) {
                statement.execute("CREATE TABLE IF NOT EXISTS test(i int)");
              } catch (Throwable ex) {
                System.out.println("Table creation for " 
                    + container + " failed: " + ex.getMessage());
              }
              
              int currentNumber;
              try (Statement statement = connection.createStatement();
                  ResultSet resultSet = statement.executeQuery("SELECT max(i) FROM test");) {
                currentNumber = resultSet.next() ? resultSet.getInt(1) : 0;
              } catch (Throwable ex) {
                System.out.println("Table select for " 
                    + container + " failed: " + ex.getMessage());
                continue;
              }
              
              if (number == null) {
                number = currentNumber;
              }
              
              if (number.intValue() != currentNumber) {
                System.out.println("Sequence for " 
                    + container + " does not match. Expected " 
                    + number + " but was " + currentNumber);
                number = currentNumber;
              }
              
              number = number + 1;
              
              try (Statement statement = connection.createStatement()) {
                statement
                    .execute("INSERT INTO test VALUES (" + number + ")");
              } catch (Throwable ex) {
                System.out.println("Table insert for " 
                    + container + " failed: " + ex.getMessage());
                number = number - 1;
              }
              
            } finally {
              try {
                connection.close();
              } catch (Throwable ex) {
                System.out.println("Closing connection " 
                    + container + " failed: " + ex.getMessage());
              }
            }
          } catch (Throwable ex) {
            System.out.println("Connection to " + container + " failed: " + ex.getMessage());
          }
          
          parent.interruptibleSleep(interval, TimeUnit.MILLISECONDS);
        }
      } catch (Throwable ex) {
        ex.printStackTrace(System.err);
      }
    }
  }

}

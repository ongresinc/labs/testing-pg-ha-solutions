# Testing PostgreSQL HA solutions 

This project was initiated as a test base of HA solutions.


## Requirements

Docker 17.x or above, Maven 3.x or above, JDK 8 or above

## Compile bot

```bash
mvn package
```

### RepMgr

#### Repmgr 3.3 PoC

```bash
mvn package -P poc-repmgr-3.3
```

#### Repmgr 4.1.0 PoC

```bash
mvn package -P poc-repmgr-4.1.0
```

### Patroni

WIP

```bash
docker-compose -f docker-compose-patroni.yml build
docker-compose -f docker-compose-patroni.yml up --scale consul-bootstrap=1 --scale consul-server=1 --scale pg=4 --scale pgb=1
```

## Run testcases

```bash
bash src/main/testcases/normal-failover.sh --repmgr-version (3.3|4.1.0)
```
